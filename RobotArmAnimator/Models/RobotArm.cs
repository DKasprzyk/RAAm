﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Models.RobotArmAnimator
{
    public class RobotArm
    {
        public enum Mode
        {
            Sequential = 0,
            Serial = 1
        }
        public string Name { get; set; }
        public List<Joint> Joints { get; set; }
        public List<double[]> Automation { get; set; }

        public double NormalChangeValue { get; set; }
        public double SlowedChangeValue { get; set; }
        public double SlowedOffset { get; set; }

        public bool Sequential { get; set; }

        public const int ROUND = 2;


        public RobotArm(string name, List<Joint> joints, double normalChangeValue, double slowedChangeValue, double slowedOffset, bool sequential)
        {
            Name = name;
            Joints = joints;
            NormalChangeValue = normalChangeValue;
            SlowedChangeValue = slowedChangeValue;
            SlowedOffset = slowedOffset;
            Sequential = sequential;
            Automation = new List<double[]>();
        }

        public void MoveAllJoints(Mode mode) 
        {
            switch (mode)
            {
                case Mode.Sequential:
                    MoveAllJointsSequential();
                    break;
                case Mode.Serial:
                    MoveAllJointsSerial();
                    break;
                default:
                    break;
            }
        }
        public void MoveAllJointsSequential()
        {
            double[] row;
            int cnt = Joints.Count;

            for (int i = 0; i < cnt; i++)
            {
                Joints[i].Move(NormalChangeValue, SlowedChangeValue, SlowedOffset);
                foreach (double operation in Joints[i].Operations)
                {
                    row = new double[cnt];
                    for (int j = 0; j < row.Length; j++)
                    {
                        if (j.Equals(i))
                        {
                            row[j] = operation;
                        }
                        else
                        {
                            row[j] = Joints[j].CurrentState;
                        }
                    }
                    Automation.Add(row);
                }
                Joints[i].Operations.Clear();
            }
        }
        public void MoveAllJointsSerial()
        {
            double[] row;
            int cnt = Joints.Count;
            Dictionary<Joint, int> toMoveWithCounts = new Dictionary<Joint, int>();
            int toMoveWithCountsMin;

            double ratio;
            double normal;
            double slowed;
            double offset;

            List<int> operationsList = new List<int>();
            int operations = 0;
            int operationsTemp;

            foreach (Joint joint in Joints)
            {
                if (joint.CanMove)
                {
                    toMoveWithCounts.Add(joint, joint.GetPredictedtOperationsCount(NormalChangeValue,SlowedChangeValue,SlowedOffset));
                }
            }

            var toMoveWithCountsSorted = from entry in toMoveWithCounts orderby entry.Value ascending select entry;
            toMoveWithCounts = toMoveWithCountsSorted.ToDictionary(pair => pair.Key, pair => pair.Value);
            toMoveWithCountsMin = toMoveWithCounts.First().Value;
            

            foreach (KeyValuePair<Joint, int> joint in toMoveWithCounts)
            {
                ratio = GetSmallertoLargerRatio(toMoveWithCountsMin, joint.Value);

                normal = NormalChangeValue * ratio;
                slowed = SlowedChangeValue * ratio;
                offset = SlowedOffset;

                operationsTemp = Joints[Joints.IndexOf(joint.Key)].GetPredictedtOperationsCount(normal, slowed, offset);
                operationsList.Add(operationsTemp);
                Joints[Joints.IndexOf(joint.Key)].Move(normal, slowed, offset);
                if (operations.Equals(0))
                {
                    operations = operationsTemp;
                }
                else 
                {
                    if (!operations.Equals(operationsTemp))
                    {
                        return;
                    }
                }
            }


            for (int i = 0; i < operations; i++)
            {
                row = new double[cnt];
                for (int j = 0; j < row.Length; j++)
                {
                    if (Joints[j].Operations.Count > 0)
                    {
                        row[j] = Joints[j].Operations[i];
                    }
                    else
                    {
                        row[j] = Joints[j].CurrentState;
                    }
                }
                Automation.Add(row);
            }

            foreach (Joint joint in Joints)
            {
                joint.Operations.Clear();
            }
        }
        public double GetSmallertoLargerRatio(double valueOne, double ValueTwo)
        {
            if (valueOne.Equals(ValueTwo))
            {
                return 1;
            }
            else
            {
                if (valueOne > ValueTwo)
                {
                    return valueOne / ValueTwo;
                }
                else
                {
                    return ValueTwo / valueOne;
                }
            }
            
        }

        public List<string> GetAutomationStringList(bool withNames = true, string separator = " ")
        {
            List<string> result = new List<string>();
            string resultRow = "";
            int len;
            if (withNames)
            {
                foreach (Joint joint in Joints)
                {
                    if (joint.Equals(Joints.Last()))
                    {
                        resultRow += $"{joint.Name}";
                    }
                    else
                    {
                        resultRow += $"{joint.Name}{separator}";
                    }
                }
                result.Add(resultRow);
            }
            resultRow = "";
            foreach (double[] row in Automation)
            {
                len = row.Length;
                for (int i = 0; i < len; i++)
                {
                    if (i.Equals(len - 1))
                    {
                        resultRow += $"{Math.Round(row[i],2)}";
                    }
                    else
                    {
                        resultRow += $"{Math.Round(row[i], ROUND)}{separator}";
                    }
                }
                result.Add(resultRow.Replace(',', '.'));
                resultRow = "";
            }
            return result;
        }
        public DataTable GetAutomationDataTable()
        {
            DataTable result = new DataTable();

            for (int i = 0; i < Joints.Count; i++)
            {
                result.Columns.Add(Joints[i].Name);
            }

            foreach (double[] set in Automation)
            {
                DataRow row = result.NewRow();
                for (int i = 0; i < set.Length; i++)
                {
                    row[i] = Math.Round(set[i], ROUND);
                }
                result.Rows.Add(row);
            }

            return result;
        }
    }
}
