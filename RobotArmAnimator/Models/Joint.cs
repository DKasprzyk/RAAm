﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Models.RobotArmAnimator
{
    public class Joint
    {
        public string Name { get; set; }
        public double JointStartingState { get; set; }
        public double CurrentState { get; set; }
        public double TargetState { get; set; }
        public double MinValue { get; set; }
        public double MaxValue { get; set; }
        public List<double> Operations { get; set; }
        public bool CanMove { 
            get 
            {
                var result = false;
                if (CurrentState != TargetState)
                {
                    result = true;
                }
                return result;
            } 
        }

        public Joint(string name, double jointStartingState, double minValue, double maxValue)
        {
            Name = name;
            MinValue = minValue;
            MaxValue = maxValue;
            JointStartingState = jointStartingState;
            CurrentState = jointStartingState;
            TargetState = jointStartingState;
            Operations = new List<double>();
        }

        public int GetPredictedtOperationsCount(double normalChangeValue, double slowedChangeValue, double slowedOffset)
        {
            Operations.Clear();

            List<double> ops = Operations;
            double cs = CurrentState;
            double ts = TargetState;

            CalculateMovement(ref ops, ref cs, ref ts, normalChangeValue, slowedChangeValue, slowedOffset);

            return ops.Count();
        }
        public void Move(double normalChangeValue, double slowedChangeValue, double slowedOffset)
        {
            Operations.Clear();

            List<double> ops = Operations;
            double cs = CurrentState;
            double ts = TargetState;

            CalculateMovement(ref ops, ref cs, ref ts, normalChangeValue, slowedChangeValue, slowedOffset);

            Operations = ops;
            CurrentState = cs;
            TargetState = ts;

        }
        public void CalculateMovement(ref List<double> ops, ref double cs, ref double ts, double normalChangeValue, double slowedChangeValue, double slowedOffset)
        {
            double startPoint = cs;
            double midlePoint;
            double endPoint;

            if (cs < ts)
            {
                midlePoint = cs + slowedOffset;
                endPoint = ts - slowedOffset;
                while (!cs.Equals(ts))
                {
                    if (cs >= startPoint && cs < midlePoint)
                    {
                        if (cs + slowedChangeValue > midlePoint)
                        {
                            cs = midlePoint;
                        }
                        else
                        {
                            cs += slowedChangeValue;
                        }
                    }
                    else if (cs >= midlePoint && cs < endPoint)
                    {
                        if (cs + normalChangeValue > endPoint)
                        {
                            cs = endPoint;
                        }
                        else
                        {
                            cs += normalChangeValue;
                        }
                    }
                    else if (cs >= endPoint && cs < ts)
                    {
                        if (cs + slowedChangeValue > ts)
                        {
                            cs = ts;
                        }
                        else
                        {
                            cs += slowedChangeValue;
                        }
                    }
                    ops.Add(cs);
                }
            }
            else if (cs > ts)
            {
                midlePoint = cs - slowedOffset;
                endPoint = ts + slowedOffset;
                while (!cs.Equals(ts))
                {
                    if (cs <= startPoint && cs > midlePoint)
                    {
                        if (cs - slowedChangeValue < midlePoint)
                        {
                            cs = midlePoint;
                        }
                        else
                        {
                            cs -= slowedChangeValue;
                        }
                    }
                    else if (cs <= midlePoint && cs > endPoint)
                    {
                        if (cs - normalChangeValue < endPoint)
                        {
                            cs = endPoint;
                        }
                        else
                        {
                            cs -= normalChangeValue;
                        }
                    }
                    else if (cs <= endPoint && cs > ts)
                    {
                        if (cs - slowedChangeValue < ts)
                        {
                            cs = ts;
                        }
                        else
                        {
                            cs -= slowedChangeValue;
                        }
                    }
                    ops.Add(cs);
                }
            }
            else
            {
                return;
            }
        }
    }
}
