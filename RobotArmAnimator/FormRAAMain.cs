﻿using Models.RobotArmAnimator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace RobotArmAnimator
{
    public partial class FormRAAMain : Form
    {
        private struct JointBindingStruct
        {
            public RobotArm Robot { get; set; }
            public int JointNo { get; set; }
            public string TargetValue { get; set; }
            public string SourceValue { get; set; }

            public JointBindingStruct(RobotArm robot, int jointNo, string targetValue, string sourceValue)
            {
                Robot = robot;
                JointNo = jointNo;
                TargetValue = targetValue;
                SourceValue = sourceValue;
            }
        }

        RobotArm RAM { get; set; }
        List<Dictionary<dynamic, JointBindingStruct>> DictList { get; set; }
        bool settingsEnabled;

        public FormRAAMain()
        {
            InitializeComponent();
            Setup();
        }

        public void Setup()
        {
            settingsEnabled = false;

            RAM = GetDefaultKuka();

            DictList = new List<Dictionary<dynamic, JointBindingStruct>>()
            {
                {
                    new Dictionary<dynamic, JointBindingStruct>()
                    {
                        {tbOne,                 new JointBindingStruct (RAM, 0, "Text",  "Name")},
                        {tbTwo,                 new JointBindingStruct (RAM, 1, "Text",  "Name")},
                        {tbThree,               new JointBindingStruct (RAM, 2, "Text",  "Name")},
                        {tbFour,                new JointBindingStruct (RAM, 3, "Text",  "Name")},
                        {tbFive,                new JointBindingStruct (RAM, 4, "Text",  "Name")},
                        {tbSix,                 new JointBindingStruct (RAM, 5, "Text",  "Name")},

                        {nudOne_min,            new JointBindingStruct (RAM, 0, "Value", "MinValue")},
                        {nudTwo_min,            new JointBindingStruct (RAM, 1, "Value", "MinValue")},
                        {nudThree_min,          new JointBindingStruct (RAM, 2, "Value", "MinValue")},
                        {nudFour_min,           new JointBindingStruct (RAM, 3, "Value", "MinValue")},
                        {nudFive_min,           new JointBindingStruct (RAM, 4, "Value", "MinValue")},
                        {nudSix_min,            new JointBindingStruct (RAM, 5, "Value", "MinValue")},

                        {nudOne_max,            new JointBindingStruct (RAM, 0, "Value", "MaxValue")},
                        {nudTwo_max,            new JointBindingStruct (RAM, 1, "Value", "MaxValue")},
                        {nudThree_max,          new JointBindingStruct (RAM, 2, "Value", "MaxValue")},
                        {nudFour_max,           new JointBindingStruct (RAM, 3, "Value", "MaxValue")},
                        {nudFive_max,           new JointBindingStruct (RAM, 4, "Value", "MaxValue")},
                        {nudSix_max,            new JointBindingStruct (RAM, 5, "Value", "MaxValue")},

                        {nudOne_operation,      new JointBindingStruct (RAM, 0, "Value", "TargetState")},
                        {nudTwo_operation,      new JointBindingStruct (RAM, 1, "Value", "TargetState")},
                        {nudThree_operation,    new JointBindingStruct (RAM, 2, "Value", "TargetState")},
                        {nudFour_operation,     new JointBindingStruct (RAM, 3, "Value", "TargetState")},
                        {nudFive_operation,     new JointBindingStruct (RAM, 4, "Value", "TargetState")},
                        {nudSix_operation,      new JointBindingStruct (RAM, 5, "Value", "TargetState")},

                        {nudIterations,         new JointBindingStruct (RAM, -1, "Value", "NormalChangeValue")},
                        {nudSlowdown,           new JointBindingStruct (RAM, -1, "Value", "SlowedChangeValue")},
                        {nudOffset,             new JointBindingStruct (RAM, -1, "Value", "SlowedOffset")},
                        {cbxSequential,         new JointBindingStruct (RAM, -1, "Checked", "Sequential")}
                    }
                },
                {
                    new Dictionary<dynamic, JointBindingStruct>()
                    {
                        {nudOne_operation,      new JointBindingStruct (RAM, 0, "Minimum", "MinValue")},
                        {nudTwo_operation,      new JointBindingStruct (RAM, 1, "Minimum", "MinValue")},
                        {nudThree_operation,    new JointBindingStruct (RAM, 2, "Minimum", "MinValue")},
                        {nudFour_operation,     new JointBindingStruct (RAM, 3, "Minimum", "MinValue")},
                        {nudFive_operation,     new JointBindingStruct (RAM, 4, "Minimum", "MinValue")},
                        {nudSix_operation,      new JointBindingStruct (RAM, 5, "Minimum", "MinValue")}
                    }
                },
                {
                    new Dictionary<dynamic, JointBindingStruct>()
                    {
                        {nudOne_operation,      new JointBindingStruct (RAM, 0, "Maximum", "MaxValue")},
                        {nudTwo_operation,      new JointBindingStruct (RAM, 1, "Maximum", "MaxValue")},
                        {nudThree_operation,    new JointBindingStruct (RAM, 2, "Maximum", "MaxValue")},
                        {nudFour_operation,     new JointBindingStruct (RAM, 3, "Maximum", "MaxValue")},
                        {nudFive_operation,     new JointBindingStruct (RAM, 4, "Maximum", "MaxValue")},
                        {nudSix_operation,      new JointBindingStruct (RAM, 5, "Maximum", "MaxValue")}
                    }
                },
            };
            
            foreach (Dictionary<dynamic, JointBindingStruct > dict in DictList)
            {
                foreach (KeyValuePair<dynamic, JointBindingStruct> item in dict)
                {
                    if (item.Value.JointNo >= 0)
                    {
                        item.Key.DataBindings.Add(new Binding(item.Value.TargetValue, item.Value.Robot.Joints[item.Value.JointNo], item.Value.SourceValue));
                    }
                    else
                    {
                        item.Key.DataBindings.Add(new Binding(item.Value.TargetValue, item.Value.Robot, item.Value.SourceValue));
                    }
                }
            }
        }
        public void PerformOperation()
        {
            if (cbxSequential.Checked)
            {
                RAM.MoveAllJoints(RobotArm.Mode.Sequential);
            }
            else
            {
                RAM.MoveAllJoints(RobotArm.Mode.Serial);
            }
        }
        public void ViewOperation()
        {
            DataSet dataSet = new DataSet();
            DataTable dt = RAM.GetAutomationDataTable();

            dt.TableName = "RAM";
            dataSet.Tables.Add(dt);
            dataGridView1.DataSource = dataSet;
            dataGridView1.DataMember = "RAM";
            dataGridView1.Refresh();
        }
        public void SaveToFile(List<string> values)
        {
            string folderPath = Directory.GetCurrentDirectory();
            string fileName = "Zadanie2.dat";
            string path = $"{folderPath}\\{fileName}";

            using (TextWriter tw = new StreamWriter(path))
            {
                foreach (string s in values)
                    tw.WriteLine(s);
            }
        }
        public RobotArm GetDefaultKuka()
        {
            return new RobotArm
            (
                "KUKA",
                new List<Joint>()
                { 
                    // KukaTheta-2 KukaTheta-3 KukaTheta-4 KukaTheta-5 KukaTheta-6
                    new Joint("KukaTheta-1", 0,-185.0,185.0),
                    new Joint("KukaTheta-2", 0,-50.0,90.0),
                    new Joint("KukaTheta-3", 0,-120.0,145.0),
                    new Joint("KukaTheta-4", 0,-350.0,350.0),
                    new Joint("KukaTheta-5", 0,-120.0,120.0),
                    new Joint("KukaTheta-6", 0,-350.0,350.0)
                },
                0.5,
                0.2,
                4,
                false
            );
        }

        private void btnConfirmOperation_Click(object sender, EventArgs e)
        {
            PerformOperation();
            ViewOperation();
        }
        private void btnConfirmSettings_Click(object sender, EventArgs e)
        {
            if (settings.Enabled)
            {
                settings.Enabled = false;
                btnConfirmSettings.Text = "Odblokuj ustawienia";
            }
            else
            {
                settings.Enabled = true;
                btnConfirmSettings.Text = "Zablokuj ustawienia";
            }
        }
        private void tsbExport_Click(object sender, EventArgs e)
        {
            SaveToFile(RAM.GetAutomationStringList());
        }
        private void tsbReset_Click(object sender, EventArgs e)
        {
            RAM.Automation.Clear();
            foreach (Joint joint in RAM.Joints)
            {
                joint.CurrentState = joint.JointStartingState;
                joint.TargetState = joint.JointStartingState;
            }
        }
    }
}
