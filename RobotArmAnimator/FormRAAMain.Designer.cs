﻿namespace RobotArmAnimator
{
    partial class FormRAAMain
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRAAMain));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MainMenu = new System.Windows.Forms.ToolStrip();
            this.tsbExport = new System.Windows.Forms.ToolStripButton();
            this.settings = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cbxSequential = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudOffset = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudSlowdown = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudIterations = new System.Windows.Forms.NumericUpDown();
            this.tbSix = new System.Windows.Forms.TextBox();
            this.nudSix_max = new System.Windows.Forms.NumericUpDown();
            this.lblSix = new System.Windows.Forms.Label();
            this.nudSix_min = new System.Windows.Forms.NumericUpDown();
            this.tbFive = new System.Windows.Forms.TextBox();
            this.nudFive_max = new System.Windows.Forms.NumericUpDown();
            this.lblFive = new System.Windows.Forms.Label();
            this.nudFive_min = new System.Windows.Forms.NumericUpDown();
            this.tbFour = new System.Windows.Forms.TextBox();
            this.nudFour_max = new System.Windows.Forms.NumericUpDown();
            this.lblFour = new System.Windows.Forms.Label();
            this.nudFour_min = new System.Windows.Forms.NumericUpDown();
            this.tbThree = new System.Windows.Forms.TextBox();
            this.nudThree_max = new System.Windows.Forms.NumericUpDown();
            this.lblThree = new System.Windows.Forms.Label();
            this.nudThree_min = new System.Windows.Forms.NumericUpDown();
            this.tbTwo = new System.Windows.Forms.TextBox();
            this.nudTwo_max = new System.Windows.Forms.NumericUpDown();
            this.lblTwo = new System.Windows.Forms.Label();
            this.nudTwo_min = new System.Windows.Forms.NumericUpDown();
            this.tbOne = new System.Windows.Forms.TextBox();
            this.nudOne_max = new System.Windows.Forms.NumericUpDown();
            this.lblOne = new System.Windows.Forms.Label();
            this.nudOne_min = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnConfirmSettings = new System.Windows.Forms.Button();
            this.btnConfirmOperation = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nudSix_operation = new System.Windows.Forms.NumericUpDown();
            this.nudFive_operation = new System.Windows.Forms.NumericUpDown();
            this.nudFour_operation = new System.Windows.Forms.NumericUpDown();
            this.nudThree_operation = new System.Windows.Forms.NumericUpDown();
            this.nudTwo_operation = new System.Windows.Forms.NumericUpDown();
            this.nudOne_operation = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.MainMenu.SuspendLayout();
            this.settings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlowdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIterations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_max)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_min)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_operation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_operation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_operation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_operation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_operation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_operation)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 53);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(929, 591);
            this.dataGridView1.TabIndex = 0;
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbExport});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.MinimumSize = new System.Drawing.Size(0, 50);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(1370, 50);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "toolStrip1";
            // 
            // tsbExport
            // 
            this.tsbExport.Image = ((System.Drawing.Image)(resources.GetObject("tsbExport.Image")));
            this.tsbExport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbExport.Name = "tsbExport";
            this.tsbExport.Size = new System.Drawing.Size(88, 47);
            this.tsbExport.Text = "Exportuj";
            this.tsbExport.Click += new System.EventHandler(this.tsbExport_Click);
            // 
            // settings
            // 
            this.settings.Controls.Add(this.label11);
            this.settings.Controls.Add(this.label10);
            this.settings.Controls.Add(this.cbxSequential);
            this.settings.Controls.Add(this.label9);
            this.settings.Controls.Add(this.nudOffset);
            this.settings.Controls.Add(this.label7);
            this.settings.Controls.Add(this.nudSlowdown);
            this.settings.Controls.Add(this.label8);
            this.settings.Controls.Add(this.nudIterations);
            this.settings.Controls.Add(this.tbSix);
            this.settings.Controls.Add(this.nudSix_max);
            this.settings.Controls.Add(this.lblSix);
            this.settings.Controls.Add(this.nudSix_min);
            this.settings.Controls.Add(this.tbFive);
            this.settings.Controls.Add(this.nudFive_max);
            this.settings.Controls.Add(this.lblFive);
            this.settings.Controls.Add(this.nudFive_min);
            this.settings.Controls.Add(this.tbFour);
            this.settings.Controls.Add(this.nudFour_max);
            this.settings.Controls.Add(this.lblFour);
            this.settings.Controls.Add(this.nudFour_min);
            this.settings.Controls.Add(this.tbThree);
            this.settings.Controls.Add(this.nudThree_max);
            this.settings.Controls.Add(this.lblThree);
            this.settings.Controls.Add(this.nudThree_min);
            this.settings.Controls.Add(this.tbTwo);
            this.settings.Controls.Add(this.nudTwo_max);
            this.settings.Controls.Add(this.lblTwo);
            this.settings.Controls.Add(this.nudTwo_min);
            this.settings.Controls.Add(this.tbOne);
            this.settings.Controls.Add(this.nudOne_max);
            this.settings.Controls.Add(this.lblOne);
            this.settings.Controls.Add(this.nudOne_min);
            this.settings.Location = new System.Drawing.Point(938, 53);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(423, 591);
            this.settings.TabIndex = 44;
            this.settings.TabStop = false;
            this.settings.Text = "Ustawienia";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(334, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 17);
            this.label11.TabIndex = 65;
            this.label11.Text = "Max";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(250, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 17);
            this.label10.TabIndex = 64;
            this.label10.Text = "Min";
            // 
            // cbxSequential
            // 
            this.cbxSequential.AutoSize = true;
            this.cbxSequential.Checked = true;
            this.cbxSequential.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxSequential.Location = new System.Drawing.Point(253, 354);
            this.cbxSequential.Name = "cbxSequential";
            this.cbxSequential.Size = new System.Drawing.Size(97, 21);
            this.cbxSequential.TabIndex = 63;
            this.cbxSequential.Text = "Sekwencja";
            this.cbxSequential.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 309);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(143, 17);
            this.label9.TabIndex = 62;
            this.label9.Text = "Moment spowolnienia";
            // 
            // nudOffset
            // 
            this.nudOffset.DecimalPlaces = 4;
            this.nudOffset.Location = new System.Drawing.Point(253, 304);
            this.nudOffset.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudOffset.Name = "nudOffset";
            this.nudOffset.Size = new System.Drawing.Size(162, 22);
            this.nudOffset.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(200, 17);
            this.label7.TabIndex = 60;
            this.label7.Text = "Spowolnienie przemieszczenia";
            // 
            // nudSlowdown
            // 
            this.nudSlowdown.DecimalPlaces = 4;
            this.nudSlowdown.Location = new System.Drawing.Point(253, 274);
            this.nudSlowdown.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudSlowdown.Name = "nudSlowdown";
            this.nudSlowdown.Size = new System.Drawing.Size(162, 22);
            this.nudSlowdown.TabIndex = 59;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(183, 17);
            this.label8.TabIndex = 58;
            this.label8.Text = "Przemieszczenie co iteracje";
            // 
            // nudIterations
            // 
            this.nudIterations.DecimalPlaces = 4;
            this.nudIterations.Location = new System.Drawing.Point(253, 246);
            this.nudIterations.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudIterations.Name = "nudIterations";
            this.nudIterations.Size = new System.Drawing.Size(162, 22);
            this.nudIterations.TabIndex = 57;
            // 
            // tbSix
            // 
            this.tbSix.Location = new System.Drawing.Point(58, 186);
            this.tbSix.Name = "tbSix";
            this.tbSix.Size = new System.Drawing.Size(189, 22);
            this.tbSix.TabIndex = 56;
            // 
            // nudSix_max
            // 
            this.nudSix_max.Location = new System.Drawing.Point(337, 186);
            this.nudSix_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudSix_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudSix_max.Name = "nudSix_max";
            this.nudSix_max.Size = new System.Drawing.Size(78, 22);
            this.nudSix_max.TabIndex = 55;
            // 
            // lblSix
            // 
            this.lblSix.AutoSize = true;
            this.lblSix.Location = new System.Drawing.Point(8, 191);
            this.lblSix.Name = "lblSix";
            this.lblSix.Size = new System.Drawing.Size(26, 17);
            this.lblSix.TabIndex = 54;
            this.lblSix.Text = "Six";
            // 
            // nudSix_min
            // 
            this.nudSix_min.Location = new System.Drawing.Point(253, 186);
            this.nudSix_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudSix_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudSix_min.Name = "nudSix_min";
            this.nudSix_min.Size = new System.Drawing.Size(78, 22);
            this.nudSix_min.TabIndex = 53;
            // 
            // tbFive
            // 
            this.tbFive.Location = new System.Drawing.Point(58, 158);
            this.tbFive.Name = "tbFive";
            this.tbFive.Size = new System.Drawing.Size(189, 22);
            this.tbFive.TabIndex = 52;
            // 
            // nudFive_max
            // 
            this.nudFive_max.Location = new System.Drawing.Point(337, 158);
            this.nudFive_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFive_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFive_max.Name = "nudFive_max";
            this.nudFive_max.Size = new System.Drawing.Size(78, 22);
            this.nudFive_max.TabIndex = 51;
            // 
            // lblFive
            // 
            this.lblFive.AutoSize = true;
            this.lblFive.Location = new System.Drawing.Point(8, 163);
            this.lblFive.Name = "lblFive";
            this.lblFive.Size = new System.Drawing.Size(34, 17);
            this.lblFive.TabIndex = 50;
            this.lblFive.Text = "Five";
            // 
            // nudFive_min
            // 
            this.nudFive_min.Location = new System.Drawing.Point(253, 158);
            this.nudFive_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFive_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFive_min.Name = "nudFive_min";
            this.nudFive_min.Size = new System.Drawing.Size(78, 22);
            this.nudFive_min.TabIndex = 49;
            // 
            // tbFour
            // 
            this.tbFour.Location = new System.Drawing.Point(58, 130);
            this.tbFour.Name = "tbFour";
            this.tbFour.Size = new System.Drawing.Size(189, 22);
            this.tbFour.TabIndex = 48;
            // 
            // nudFour_max
            // 
            this.nudFour_max.Location = new System.Drawing.Point(337, 131);
            this.nudFour_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFour_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFour_max.Name = "nudFour_max";
            this.nudFour_max.Size = new System.Drawing.Size(78, 22);
            this.nudFour_max.TabIndex = 47;
            // 
            // lblFour
            // 
            this.lblFour.AutoSize = true;
            this.lblFour.Location = new System.Drawing.Point(8, 136);
            this.lblFour.Name = "lblFour";
            this.lblFour.Size = new System.Drawing.Size(37, 17);
            this.lblFour.TabIndex = 46;
            this.lblFour.Text = "Four";
            // 
            // nudFour_min
            // 
            this.nudFour_min.Location = new System.Drawing.Point(253, 131);
            this.nudFour_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFour_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFour_min.Name = "nudFour_min";
            this.nudFour_min.Size = new System.Drawing.Size(78, 22);
            this.nudFour_min.TabIndex = 45;
            // 
            // tbThree
            // 
            this.tbThree.Location = new System.Drawing.Point(58, 102);
            this.tbThree.Name = "tbThree";
            this.tbThree.Size = new System.Drawing.Size(189, 22);
            this.tbThree.TabIndex = 44;
            // 
            // nudThree_max
            // 
            this.nudThree_max.Location = new System.Drawing.Point(337, 100);
            this.nudThree_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudThree_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudThree_max.Name = "nudThree_max";
            this.nudThree_max.Size = new System.Drawing.Size(78, 22);
            this.nudThree_max.TabIndex = 43;
            // 
            // lblThree
            // 
            this.lblThree.AutoSize = true;
            this.lblThree.Location = new System.Drawing.Point(8, 107);
            this.lblThree.Name = "lblThree";
            this.lblThree.Size = new System.Drawing.Size(46, 17);
            this.lblThree.TabIndex = 42;
            this.lblThree.Text = "Three";
            // 
            // nudThree_min
            // 
            this.nudThree_min.Location = new System.Drawing.Point(253, 100);
            this.nudThree_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudThree_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudThree_min.Name = "nudThree_min";
            this.nudThree_min.Size = new System.Drawing.Size(78, 22);
            this.nudThree_min.TabIndex = 41;
            // 
            // tbTwo
            // 
            this.tbTwo.Location = new System.Drawing.Point(58, 74);
            this.tbTwo.Name = "tbTwo";
            this.tbTwo.Size = new System.Drawing.Size(189, 22);
            this.tbTwo.TabIndex = 40;
            // 
            // nudTwo_max
            // 
            this.nudTwo_max.Location = new System.Drawing.Point(337, 72);
            this.nudTwo_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudTwo_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudTwo_max.Name = "nudTwo_max";
            this.nudTwo_max.Size = new System.Drawing.Size(78, 22);
            this.nudTwo_max.TabIndex = 39;
            // 
            // lblTwo
            // 
            this.lblTwo.AutoSize = true;
            this.lblTwo.Location = new System.Drawing.Point(8, 79);
            this.lblTwo.Name = "lblTwo";
            this.lblTwo.Size = new System.Drawing.Size(34, 17);
            this.lblTwo.TabIndex = 38;
            this.lblTwo.Text = "Two";
            // 
            // nudTwo_min
            // 
            this.nudTwo_min.Location = new System.Drawing.Point(253, 72);
            this.nudTwo_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudTwo_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudTwo_min.Name = "nudTwo_min";
            this.nudTwo_min.Size = new System.Drawing.Size(78, 22);
            this.nudTwo_min.TabIndex = 37;
            // 
            // tbOne
            // 
            this.tbOne.Location = new System.Drawing.Point(58, 46);
            this.tbOne.Name = "tbOne";
            this.tbOne.Size = new System.Drawing.Size(189, 22);
            this.tbOne.TabIndex = 36;
            // 
            // nudOne_max
            // 
            this.nudOne_max.Location = new System.Drawing.Point(337, 46);
            this.nudOne_max.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudOne_max.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudOne_max.Name = "nudOne_max";
            this.nudOne_max.Size = new System.Drawing.Size(78, 22);
            this.nudOne_max.TabIndex = 35;
            // 
            // lblOne
            // 
            this.lblOne.AutoSize = true;
            this.lblOne.Location = new System.Drawing.Point(8, 51);
            this.lblOne.Name = "lblOne";
            this.lblOne.Size = new System.Drawing.Size(35, 17);
            this.lblOne.TabIndex = 34;
            this.lblOne.Text = "One";
            // 
            // nudOne_min
            // 
            this.nudOne_min.Location = new System.Drawing.Point(253, 46);
            this.nudOne_min.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudOne_min.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudOne_min.Name = "nudOne_min";
            this.nudOne_min.Size = new System.Drawing.Size(78, 22);
            this.nudOne_min.TabIndex = 33;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnConfirmSettings);
            this.groupBox2.Controls.Add(this.btnConfirmOperation);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.nudSix_operation);
            this.groupBox2.Controls.Add(this.nudFive_operation);
            this.groupBox2.Controls.Add(this.nudFour_operation);
            this.groupBox2.Controls.Add(this.nudThree_operation);
            this.groupBox2.Controls.Add(this.nudTwo_operation);
            this.groupBox2.Controls.Add(this.nudOne_operation);
            this.groupBox2.Location = new System.Drawing.Point(0, 650);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1361, 101);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Główny panel operacji";
            // 
            // btnConfirmSettings
            // 
            this.btnConfirmSettings.Location = new System.Drawing.Point(949, 29);
            this.btnConfirmSettings.Name = "btnConfirmSettings";
            this.btnConfirmSettings.Size = new System.Drawing.Size(404, 48);
            this.btnConfirmSettings.TabIndex = 68;
            this.btnConfirmSettings.Text = "Zablokuj ustawienia";
            this.btnConfirmSettings.UseVisualStyleBackColor = true;
            this.btnConfirmSettings.Click += new System.EventHandler(this.btnConfirmSettings_Click);
            // 
            // btnConfirmOperation
            // 
            this.btnConfirmOperation.Location = new System.Drawing.Point(653, 30);
            this.btnConfirmOperation.Name = "btnConfirmOperation";
            this.btnConfirmOperation.Size = new System.Drawing.Size(270, 47);
            this.btnConfirmOperation.TabIndex = 46;
            this.btnConfirmOperation.Text = "Wykonaj operacje";
            this.btnConfirmOperation.UseVisualStyleBackColor = true;
            this.btnConfirmOperation.Click += new System.EventHandler(this.btnConfirmOperation_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(544, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 17);
            this.label1.TabIndex = 54;
            this.label1.Text = "Six";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(438, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 53;
            this.label2.Text = "Five";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(332, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 17);
            this.label3.TabIndex = 52;
            this.label3.Text = "Four";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(226, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 51;
            this.label4.Text = "Three";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(115, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 17);
            this.label5.TabIndex = 50;
            this.label5.Text = "Two";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "One";
            // 
            // nudSix_operation
            // 
            this.nudSix_operation.Location = new System.Drawing.Point(547, 56);
            this.nudSix_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudSix_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudSix_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudSix_operation.Name = "nudSix_operation";
            this.nudSix_operation.Size = new System.Drawing.Size(100, 22);
            this.nudSix_operation.TabIndex = 48;
            // 
            // nudFive_operation
            // 
            this.nudFive_operation.Location = new System.Drawing.Point(441, 56);
            this.nudFive_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFive_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFive_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudFive_operation.Name = "nudFive_operation";
            this.nudFive_operation.Size = new System.Drawing.Size(100, 22);
            this.nudFive_operation.TabIndex = 47;
            // 
            // nudFour_operation
            // 
            this.nudFour_operation.Location = new System.Drawing.Point(335, 56);
            this.nudFour_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudFour_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudFour_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudFour_operation.Name = "nudFour_operation";
            this.nudFour_operation.Size = new System.Drawing.Size(100, 22);
            this.nudFour_operation.TabIndex = 46;
            // 
            // nudThree_operation
            // 
            this.nudThree_operation.Location = new System.Drawing.Point(229, 56);
            this.nudThree_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudThree_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudThree_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudThree_operation.Name = "nudThree_operation";
            this.nudThree_operation.Size = new System.Drawing.Size(100, 22);
            this.nudThree_operation.TabIndex = 45;
            // 
            // nudTwo_operation
            // 
            this.nudTwo_operation.Location = new System.Drawing.Point(118, 56);
            this.nudTwo_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudTwo_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudTwo_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudTwo_operation.Name = "nudTwo_operation";
            this.nudTwo_operation.Size = new System.Drawing.Size(100, 22);
            this.nudTwo_operation.TabIndex = 44;
            // 
            // nudOne_operation
            // 
            this.nudOne_operation.Location = new System.Drawing.Point(12, 56);
            this.nudOne_operation.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudOne_operation.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
            this.nudOne_operation.MinimumSize = new System.Drawing.Size(100, 0);
            this.nudOne_operation.Name = "nudOne_operation";
            this.nudOne_operation.Size = new System.Drawing.Size(100, 22);
            this.nudOne_operation.TabIndex = 43;
            // 
            // FormRAAMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 748);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.MainMenu);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormRAAMain";
            this.Text = "Robot Arm Animator - RAA";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.settings.ResumeLayout(false);
            this.settings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSlowdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIterations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_max)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_min)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSix_operation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFive_operation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFour_operation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThree_operation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTwo_operation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOne_operation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip MainMenu;
        private System.Windows.Forms.ToolStripButton tsbExport;
        private System.Windows.Forms.GroupBox settings;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudSlowdown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudIterations;
        private System.Windows.Forms.TextBox tbSix;
        private System.Windows.Forms.NumericUpDown nudSix_max;
        private System.Windows.Forms.Label lblSix;
        private System.Windows.Forms.NumericUpDown nudSix_min;
        private System.Windows.Forms.TextBox tbFive;
        private System.Windows.Forms.NumericUpDown nudFive_max;
        private System.Windows.Forms.Label lblFive;
        private System.Windows.Forms.NumericUpDown nudFive_min;
        private System.Windows.Forms.TextBox tbFour;
        private System.Windows.Forms.NumericUpDown nudFour_max;
        private System.Windows.Forms.Label lblFour;
        private System.Windows.Forms.NumericUpDown nudFour_min;
        private System.Windows.Forms.TextBox tbThree;
        private System.Windows.Forms.NumericUpDown nudThree_max;
        private System.Windows.Forms.Label lblThree;
        private System.Windows.Forms.NumericUpDown nudThree_min;
        private System.Windows.Forms.TextBox tbTwo;
        private System.Windows.Forms.NumericUpDown nudTwo_max;
        private System.Windows.Forms.Label lblTwo;
        private System.Windows.Forms.NumericUpDown nudTwo_min;
        private System.Windows.Forms.TextBox tbOne;
        private System.Windows.Forms.NumericUpDown nudOne_max;
        private System.Windows.Forms.Label lblOne;
        private System.Windows.Forms.NumericUpDown nudOne_min;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnConfirmOperation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudSix_operation;
        private System.Windows.Forms.NumericUpDown nudFive_operation;
        private System.Windows.Forms.NumericUpDown nudFour_operation;
        private System.Windows.Forms.NumericUpDown nudThree_operation;
        private System.Windows.Forms.NumericUpDown nudTwo_operation;
        private System.Windows.Forms.NumericUpDown nudOne_operation;
        private System.Windows.Forms.CheckBox cbxSequential;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudOffset;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnConfirmSettings;
    }
}

